module basic-rest-api

go 1.14

require (
	github.com/gofiber/fiber v1.14.5
	github.com/jinzhu/gorm v1.9.16
)
