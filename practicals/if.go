// One thing to be noted is that num is available only for access from inside the if and else. i.e. the scope of num is limited to the if else blocks. If we try to access num from outside the if or else, the compiler will complain. 

package main

import (  
    "fmt"
)

func main() {  
    if num := 10; num % 2 == 0 { //checks if number is even
        fmt.Println(num,"is even") 
    }  else {
        fmt.Println(num,"is odd")
    }
}

// The else statement should start in the same line after the closing curly brace } of the if statement. If not the compiler will complain.

// - The reason is because of the way Go inserts semicolons automatically. You can read about the semicolon insertion rule here

// - The idiomatic way of writing the above program in Go's philosophy is to avoid the else and return from the if if the condition is true.
