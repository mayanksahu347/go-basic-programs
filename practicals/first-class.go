/*
A language which supports first class functions allows functions to be assigned to variables, passed as arguments to other functions and returned from other functions. Go has support for first class functions.
*/

package main

import (
	"fmt"
)

// new function type add which accepts two integer arguments and returns a integer.
type add func(a int, b int) int

// accepting function as an argument
func simple(a func(a, b int) int) {
	fmt.Println(a(60, 7))
}

// returning function
func simple1() func(a, b int) int {
	f := func(a, b int) int {
		return a + b
	}
	return f
}

func main() {
	a := func() {
		fmt.Println("hello world first class function")
	}
	// the function assigned to a does not have a name. These kind of functions are called anonymous functions since they do not have a name.
	a()
	fmt.Printf("%T\n", a)

	// call anonymous function without assigning them to a variable
	func() {
		fmt.Println("hello world first class function")
	}()

	// passing argument to anonymous function
	func(n string) {
		fmt.Println("Welcome", n)
	}("Gophers")

	//  defining function
	var c add = func(a int, b int) int {
		return a + b
	}
	s := c(5, 6)
	fmt.Println("Sum", s)

	// passing function
	f := func(a, b int) int {
		return a + b
	}
	simple(f)

	// returning function
	s1 := simple1()
	fmt.Println(s1(60, 7))

	// Closures
	/*
		Closures are a special case of anonymous functions. Closures are anonymous functions which access the variables defined outside the body of the function.
	*/

	a2 := 5
	func() {
		fmt.Println("a =", a2)
	}()

	// practical use of first-class function

	ss1 := student{
		firstName: "Naveen",
		lastName:  "Ramanathan",
		grade:     "A",
		country:   "India",
	}
	ss2 := student{
		firstName: "Samuel",
		lastName:  "Johnson",
		grade:     "B",
		country:   "USA",
	}
	studentSlice := []student{ss1, ss2}
	ff1 := filter(studentSlice, func(s student) bool {
		if s.grade == "B" {
			return true
		}
		return false
	})
	fmt.Println(ff1)

	cc := filter(studentSlice, func(s student) bool {
		if s.country == "India" {
			return true
		}
		return false
	})
	fmt.Println(cc)
}

type student struct {
	firstName string
	lastName  string
	grade     string
	country   string
}

func filter(s []student, f func(student) bool) []student {
	var r []student
	for _, v := range s {
		if f(v) == true {
			r = append(r, v)
		}
	}
	return r
}
