package main

import (  
	"fmt"
	"math"
)

func main() {  
    const a = 50
	fmt.Println(a)
	
	const (
        name = "John"
        age = 50
        country = "Canada"
    )
    fmt.Println(name)
    fmt.Println(age)
	fmt.Println(country)
	

	// * The value of a constant should be known at compile time.
	var a1 = math.Sqrt(4)   //allowed
	fmt.Println(a1)
	// const b1 = math.Sqrt(4) //not allowed
	

	// * The answer is untyped constants have a default type associated with them and they supply it if and only if a line of code demands it. In the statement var name = n in line no. 8, name needs a type and it gets it from the default type of the string constant n which is a string.
	const n = "Sam"
	fmt.Printf("type %T value %v\n", n, n)
    var names = n
	fmt.Printf("type %T value %v\n", names, names)
	

	const typedhello string = "Hello World"  
	fmt.Printf("type %T value %v\n", typedhello, typedhello)


	// * 
	var defaultName = "Sam" //allowed
	fmt.Printf("type %T value %v\n", defaultName, defaultName)
	type myString string // mystring is not an expression or you can say its a type we defined, its an alias of string
    var customName myString = "Sam" //allowed
	fmt.Printf("type %T value %v\n", customName, customName)
	// customName = defaultName //not allowed
	

	// Boolean Constants
	// const trueConst = true
    // type myBool bool
    // var defaultBool = trueConst //allowed
    // var customBool myBool = trueConst //allowed
	// defaultBool = customBool //not allowed
	
	// numeric constants
	// ** constants are untyped
	const a2 = 5
    var intVar int = a2
    var int32Var int32 = a2
    var float64Var float64 = a2
	var complex64Var complex64 = a2
	fmt.Printf("AAAA %T\n", intVar);
	fmt.Printf("AAAA %T\n", int32Var);
	fmt.Printf("AAAA %T\n", float64Var);
	fmt.Printf("AAAA %T\n", complex64Var);
    fmt.Println("intVar",intVar, "\nint32Var", int32Var, "\nfloat64Var", float64Var, "\ncomplex64Var",complex64Var)

}