// func functionname(parametername type) returntype {  
 	//function body
//    }

/* If consecutive parameters are of the same type, we can avoid writing the type each time and it is enough to be written once at the end.ie price int, no int can be written as price, no int
*/

package main

import (  
    "fmt"
)

func calculateBill(price, no int) int {  
    var totalPrice = price * no
    return totalPrice
}

func rectProps(length, width float64)(float64, float64) {  
	// returning multiple parameters
    var area = length * width
    var perimeter = (length + width) * 2
    return area, perimeter
}

// Named return values
func rectProps1(length, width float64)(area1, perimeter1 float64) {  
    area1 = length * width
    perimeter1 = (length + width) * 2
    return //no explicit return value
}

func main() {  
	price, no := 90, 6
    totalPrice := calculateBill(price, no)
    fmt.Println("Total price is", totalPrice)
	
	area, perimeter := rectProps(10.8, 5.6)
	fmt.Printf("Area %f Perimeter %f\n", area, perimeter) 
	
	area2, perimeter2 := rectProps1(10.8, 5.6)
	fmt.Printf("Area %f Perimeter %f\n", area2, perimeter2) 
}
