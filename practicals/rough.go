// package main

// import "fmt"

// func main() {
// 	a := []string{"a", "b", "c", "d"}
// 	b := &a
// 	m := addString(b)
// 	fmt.Println(m)

// }

// func addString(x *[]string) []string {
// 	_ = append(*x, "hogyaappend")
// 	(*x)[0] = "mayank"
// 	fmt.Printf("%T\n", *x)
// 	return *x
// }
//////////////////////////////////////////////////////////////////////////////

package main

import "fmt"

func main() {
	sliceS := []int{2, 4, 6, 5, 8}
	multiplyByFive := func(n int) int {
		return n * 5
	}

	s := performOperation(sliceS, multiplyByFive)
	fmt.Println(s)
}

func performOperation(x []int, m func(a int) int) []int {
	for i, v := range x {
		x[i] = m(v)
	}
	return x
}
