// A slice is a convenient, flexible and powerful wrapper on top of an array. Slices do not own any data on their own. They are the just references to existing arrays.

package main

import (
	"fmt"
)

func main() {
	a := [5]int{76, 77, 78, 79, 80}
	var b []int = a[1:4] //creates a slice from a[1] to a[3]
	fmt.Printf("%d", b)

	// updating array using slice
	d_arr := [...]int{57, 89, 90, 82, 100, 78, 67, 69, 59}
	d_slice := d_arr[2:5]
	fmt.Println("array before", d_arr)
	for i := range d_slice {
		d_slice[i]++
	}
	fmt.Println("array after", d_arr)

	// When a number of slices share the same underlying array, the changes that each one makes will be reflected in the array.

	num := [3]int{78, 79, 80}
	num1 := num[:] //creates a slice which contains all elements of the array
	num2 := num[:]

	fmt.Println("array before change 1", num)
	num1[0] = 100
	fmt.Println("array after modification to slice num1", num)
	num2[1] = 101
	fmt.Println("array after modification to slice num2", num)

	// length and capacity of slice
	fruitarray := [...]string{"apple", "orange", "grape", "mango", "water melon", "pine apple", "banana"}
	fruitslice := fruitarray[1:3]
	fmt.Printf("length of slice %d capacity %d\n", len(fruitslice), cap(fruitslice)) //length of fruitslice is 2 and capacity is 6

	fmt.Println(fruitslice[1])

	//re-slicing furitslice till its capacity
	fmt.Println(fruitslice)
	fruitslice = fruitslice[:cap(fruitslice)]
	fmt.Println(fruitslice)

	// creating a slice using make: func make([]T, len, cap) []T can be used to create a slice by passing the type, length and capacity.
	// The make function creates an array and returns a slice reference to it.
	i := make([]int, 2, 5)
	fmt.Println("aaa", i)

	// Appending to Slice

	// Well what happens under the hood is, when new elements are appended to the slice, a new array is created. The elements of the existing array are copied to this new array and a new slice reference for this new array is returned. The capacity of the new slice is now twice that of the old slice. Pretty cool right :). The following program will make things clear.

	cars := []string{"Ferrari", "Honda", "Ford"}
	fmt.Println("cars:", cars, "has old length", len(cars), "and capacity", cap(cars)) //capacity of cars is 3
	cars = append(cars, "Toyota")
	fmt.Println("cars:", cars, "has new length", len(cars), "and capacity", cap(cars)) //capacity of cars is doubled to 6

	// The zero value of a slice type is nil. A nil slice has length and capacity 0. It is possible to append values to a nil slice using the append function

	var names []string //zero value of a slice is nil
	if names == nil {
		fmt.Println("slice is nil going to append")
		names = append(names, "John", "Sebastian", "Vinay")
		fmt.Println("names contents:", names)
	}

	// It is also possible to append one slice to another using the ... operator.
	veggies := []string{"potatoes", "tomatoes", "brinjal"}
	fruits := []string{"oranges", "apples"}
	food := append(veggies, fruits...)
	fmt.Println("food:", food)

	// Passing a slice to a function
	/*
				type slice struct {
					Length        int
					Capacity      int
					ZerothElement *byte
				}

		When a slice is passed to a function, even though it's passed by value, the pointer variable will refer to the same underlying array.
	*/

	// Similar to arrays, slices can have multiple dimensions.

	pls := [][]string{
		{"C", "C++"},
		{"JavaScript"},
		{"Go", "Rust"},
	}
	for _, v1 := range pls {
		for _, v2 := range v1 {
			fmt.Printf("%s ", v2)
		}
		fmt.Printf("\n")
	}

	// Memory optimization
	// One way to solve this problem is to use the copy function func copy(dst, src []T) int to make a copy of that slice. This way we can use the new slice and the original array can be garbage collected.

	countries := []string{"USA", "Singapore", "Germany", "India", "Australia"}
	neededCountries := countries[:len(countries)-2]
	countriesCpy := make([]string, len(neededCountries))
	copy(countriesCpy, neededCountries) //copies neededCountries to countriesCpy
	fmt.Println(countriesCpy)

}

//  NOTE: nil vs empty slice
// nil slice:   [nil][0][0]
// empty slice: [addr][0][0]