package main

import (
	"fmt"
)

func main() {
	// declaring pointers
	b := 255
	var a *int = &b
	fmt.Printf("Type of a is %T\n", a)
	fmt.Println("address of b is", a)

	// Zero value of a pointer is nil
	var c *int
	if c == nil {
		fmt.Println("Gotcha", c)
		c = &b
		fmt.Printf("Gotcha after initialization: %d", c)
	}

	// Creating pointers using the new function
	size := new(int)
	fmt.Printf("\n\nSize value is %d, type is %T, address is %v\n", *size, size, size)
	*size = 85
	fmt.Println("New size value is", *size)

	/*
		Dereferencing a pointer
		Dereferencing a pointer means accessing the value of the variable which the pointer points to. *a is the syntax to deference a

	*/

	bb := 255
	_ = bb
	aa := &b
	fmt.Println("address of bb is", aa)
	fmt.Println("value of bb is", *aa)

	// passing pointers to function
	change(aa)
	fmt.Println("value of bb after passing is", *aa)

	// Returning pointer from a function
	val := getPointer()
	fmt.Println("value at pointer returned ", *val)

	// To update array element: Do not pass a pointer to an array as a argument to a function. Use slice instead.
	var a1 [3]int
	a1[0] = 10
	a1[1] = 20
	a1[2] = 30
	fmt.Println(a1)

	var pArr *[3]int = &a1
	updateArrayByPointer(pArr)
	fmt.Println(a1)

	// Using slice to update array
	updateArrayBySlices(a1[:])
	fmt.Println(a1)
}

func updateArrayBySlices(a []int) {
	a[0] = 55
}

func updateArrayByPointer(pArr *[3]int) {
	(*pArr)[0] = 55
}

func change(val *int) {
	*val = 355
}

func getPointer() *int {
	var i int = 5
	return &i
}
