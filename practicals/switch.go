package main

import (
	"fmt"
	"math/rand"
)

func number() int {
	num := 15 * 5
	return num
}

func main() {
	letter := "i"
	fmt.Printf("Letter %s is a ", letter)
	switch letter {
	case "a", "e", "i", "o", "u": //multiple expressions in case
		fmt.Println("vowel")
	default:
		fmt.Println("not a vowel")
	}

	// A fallthrough statement is used to transfer control to the first statement of the case that is present immediately after the case which has been executed.

	switch num := number(); { //num is not a constant
	case num < 50:
		fmt.Printf("%d is lesser than 50\n", num)
		fallthrough
	case num < 100:
		fmt.Printf("%d is lesser than 100\n", num)
		fallthrough
	case num < 200:
		fmt.Printf("%d is lesser than 200\n", num)
	}

	// When the switch case is inside a for loop, there might be a need to terminate the for loop early. This can be done by labeling the for loop and breaking the for loop using that label inside the switch statement.

randloop:
	for {
		switch i := rand.Intn(100); {
		case i%2 != 0:
			fmt.Printf("Generated odd number %d\n", i)
		case i%2 == 0:
			fmt.Printf("Generated even number %d\n", i)
			break randloop
		}
	}

	// Please note that if the break statement is used without the label, the switch statement will only be broken and the loop will continue running. So labeling the loop and using it in the break statement inside the switch is necessary to break the outer for loop.
}
