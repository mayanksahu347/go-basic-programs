/*
- Channels are pipes that link between goroutines within your Go based applications that allow communication and subsequently the passing of values to and from variables.
- Using a traditional channel within your goroutines can sometimes lead to issues with behavior that you may not quite be expecting.
- With traditional unbuffered channels, whenever one goroutine sends a value to this channel, that goroutine will subsequently block until the value is received from the channel.
*/

package main

import (
	"fmt"
	"math/rand"
	"time"
)

func CalculateValue(values chan int) {
	value := rand.Intn(10)
	fmt.Println("Calculated Random Value: {}", value)
	values <- value
}

// unbuffered channel
func CalculateValue1(c chan int) {
	value := rand.Intn(10)
	fmt.Println("Calculated Random Value: {}", value)
	time.Sleep(1000 * time.Millisecond)
	c <- value
	fmt.Println("Only Executes after another goroutine performs a receive on the channel", value)
}

func main() {
	fmt.Println("Go Channel Tutorial")

	// 1.
	// values := make(chan int)
	// defer close(values)

	// go CalculateValue(values)

	// value := <-values
	// fmt.Println(value)

	// 2.
	valueChannel := make(chan int)
	defer close(valueChannel)

	go CalculateValue1(valueChannel)
	go CalculateValue1(valueChannel)

	values := <-valueChannel
	fmt.Println(values)

	// buffered channel
	
}
