/*
- Polymorphism in Go is achieved with the help of interfaces. As we have already discussed, interfaces can be implicitly implemented in Go. A type implements an interface if it provides definitions for all the methods declared in the interface.
- Any type which defines all the methods of an interface is said to implicitly implement that interface.

- A variable of type interface can hold any value which implements the interface. This property of interfaces is used to achieve polymorphism in Go.
*/

// seen example in interface 1	
