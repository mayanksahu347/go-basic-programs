/*
Reflection is the ability of a program to inspect its variables and values at run time and find their type.

question: What is the need to inspect a variable and find its type?
-
*/

package main

import (
	"fmt"
	"reflect"
)

type order struct {
	ordId      int
	customerId int
}

type employee struct {
	name    string
	id      int
	address string
	salary  int
	country string
}

// basic, no use of reflection
func createQuery(o order) string {
	i := fmt.Sprintf("insert into order values(%d, %d)", o.ordId, o.customerId)
	return i
}

// reflection methods
func createQuery1(q interface{}) {
	t := reflect.TypeOf(q)
	v := reflect.ValueOf(q)
	k := t.Kind()

	fmt.Println("Type ", t)
	fmt.Println("Value ", v)
	fmt.Println("Kind ", k)

	// Type represents the actual type of the interface{}, in this case main.Order and Kind represents the specific kind of the type. In this case, it's a struct.
}

// NumField() and Field() methods
func createQuery2(q interface{}) {
	if reflect.ValueOf(q).Kind() == reflect.Struct {
		v := reflect.ValueOf(q)
		fmt.Println("Number", v)
		fmt.Println("Number of fields", v.NumField())
		for i := 0; i < v.NumField(); i++ {
			fmt.Printf("Field:%d type:%T value:%v\n", i, v.Field(i), v.Field(i))
		}
	}

	// The NumField() method returns the number of fields in a struct and the Field(i int) method returns the reflect.Value of the ith field.

	// NumField method works only on struct.
}

// Int() and String() methods: check main function

func completeCreateQuery(q interface{}) {
	if reflect.ValueOf(q).Kind() == reflect.Struct {
		t := reflect.TypeOf(q).Name()
		query := fmt.Sprintf("insert into %s values(", t)
		v := reflect.ValueOf(q)
		for i := 0; i < v.NumField(); i++ {
			switch v.Field(i).Kind() {
			case reflect.Int:
				if i == 0 {
					query = fmt.Sprintf("%s%d", query, v.Field(i).Int())
				} else {
					query = fmt.Sprintf("%s, %d", query, v.Field(i).Int())
				}
			case reflect.String:
				if i == 0 {
					query = fmt.Sprintf("%s\"%s\"", query, v.Field(i).String())
				} else {
					query = fmt.Sprintf("%s, \"%s\"", query, v.Field(i).String())
				}
			default:
				fmt.Println("Unsupported type")
				return
			}
		}
		query = fmt.Sprintf("%s)", query)
		fmt.Println(query)
		return

	}
	fmt.Println("unsupported type")
}

func main() {
	// this works fine no need of reflection here
	o := order{
		ordId:      1234,
		customerId: 12,
	}
	fmt.Println(createQuery(o))

	// query that takes any struct as argument and creates an insert query based on the struct fields.
	/* reflect package
	The reflect package implements run-time reflection in Go. The reflect package helps to identify the underlying concrete type and the value of a interface{} variable.
	*/

	// reflect methods

	o1 := order{
		ordId:      456,
		customerId: 10,
	}

	_ = o1
	// checking type, kind, value
	createQuery1(o1)

	// checking numfield
	createQuery2(o1)

	// Int() and String() methods
	// The methods Int and String help extract the reflect.Value as an int64 and string respectively.
	a := 56
	x := reflect.ValueOf(a).Int()
	fmt.Printf("type:%T value:%v\n", x, x)
	b := "Naveen"
	y := reflect.ValueOf(b).String()
	fmt.Printf("type:%T value:%v\n", y, y)

	// complete query testing
	oo := order{
		ordId:      456,
		customerId: 56,
	}
	completeCreateQuery(oo)

	ee := employee{
		name:    "Naveen",
		id:      565,
		address: "Coimbatore",
		salary:  90000,
		country: "India",
	}
	completeCreateQuery(ee)
	i := 90
	completeCreateQuery(i)
}
