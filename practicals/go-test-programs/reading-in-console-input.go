package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Simple Shell")
	fmt.Println("---------------------")

	for {
		fmt.Print("-> ")
		name, _ := reader.ReadString('\n')
		text, _ := reader.ReadString('\n')
		// convert CRLF to LF
		text = strings.Replace(text, "\n", "", -1)

		if strings.Compare("hi", text) == 0 {
			fmt.Println("hello", name)
		}

	}

	// fmt.Println("hello")

	// readera := bufio.NewReader(os.Stdin)
	// char, _, err := readera.ReadRune()

	// if err != nil {
	// 	fmt.Println(err)
	// }

	// print out the unicode value i.e. A -> 65, a -> 97
	// fmt.Println(char)

}
