package main

import "strings"

func main() {

	sampleString := "This is a sample string"
	compareString := "this is a sample string"

	// case sensitive
	if sampleString == compareString {
		println("The strings match")
	} else {
		println("Strings do not match")
	}

	// case sensitive: using string package
	if strings.Compare(sampleString, compareString) == 0 {
		println("The strings match")
	} else {
		println("Strings do not match")
	}

	// case insensitive
	if strings.ToLower(sampleString) == strings.ToLower(compareString) {
		println("The strings match")
	} else {
		println("Strings do not match")
	}

	if strings.Compare(strings.ToLower(sampleString), strings.ToLower(compareString)) == 0 {
		println("The strings match")
	} else {
		println("Strings do not match")
	}

	// using equalfold : It compares the string content while ignoring the case.
	if strings.EqualFold(sampleString, compareString) {
		println("The strings match")
	} else {
		println("Strings do not match")
	}
}
