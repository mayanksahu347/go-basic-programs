package main

import (
	"fmt"
	"os"
)

func main() {
	x("Mayank", "Sahu")
}

func x(a ...interface{}) (n int, err error) {
	return fmt.Printf(os.Stdout, a...)
}
