package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)

func compareOperators(a string, b string) bool {
	// compare with operators
	// 0.23s user 0.11s system 137% cpu 0.245 total
	if a == b {
		return true
	}
	return false
}

func compareString(a string, b string) bool {
	// 0.19s user 0.09s system 151% cpu 0.188 total
	if strings.Compare(a, b) == 0 {
		return true
	}
	return false
}

func main() {

	sampleString := "sahu"

	file, err := os.Open("names.txt")

	if err != nil {
		log.Fatalf("failed opening file: %s", err)
	}

	scanner := bufio.NewScanner(file)
	scanner.Split(bufio.ScanLines)

	found := false
	fmt.Println(scanner.Text())
	for scanner.Scan() {
		fmt.Println(scanner.Text())
		if compareString(sampleString, scanner.Text()) {
			found = true
			fmt.Printf("We found a match for %s\n", scanner.Text(), "\n")
		}
	}
	if !found {
		fmt.Printf("Not found\n")
	}
	file.Close()

}
