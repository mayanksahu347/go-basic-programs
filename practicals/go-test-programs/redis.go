package main

import (
	"encoding/json"
	"fmt"

	"github.com/go-redis/redis"
)

type user struct {
	Firstname string
	Lastname  string
	Job       string
	Age       int
}

func setValue(client *redis.Client, key string, value interface{}) error {
	return client.Set(key, value, 0).Err()
}

func getValue(client *redis.Client, key string) (string, error) {
	return client.Get(key).Result()
}

func main() {
	fmt.Println("Go Redis Tutorial")
	client := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})

	ping, err := client.Ping().Result()
	if err != nil {
		fmt.Println("Error while creating client", err)
	}

	fmt.Println(ping)

	err = setValue(client, "names", "Mayank Sahu")
	if err != nil {
		fmt.Println("Error while setting value", err)
	}

	value, err := getValue(client, "name")
	if err != nil {
		fmt.Println("Error while getting value", err)
	}

	fmt.Println("Value", value)

	// setting json corresponding to a key
	var jsonValue []uint8
	u1 := user{"Mayank", "Sahu", "Developer", 24}

	jsonValue, err = json.MarshalIndent(u1, "", " ")
	if err != nil {
		fmt.Println("Error while creating json", err)
	}

	setValue(client, "UserDetails", jsonValue)

	jValue, err := getValue(client, "UserDetails")
	if err != nil {
		fmt.Println("Error while getting json value", err)
	}

	fmt.Println("JSON Value", jValue)
}
