package main

import (
	"fmt"
	"sync"
)

//ItemType - The type of item in stack.
// type ItemType interface{}

//Stack - Stack of items.
type Stack struct {
	// Slice of type ItemType, it holds items in stack.
	items []int

	// rwLock for handling concurrent operations on the stack.
	rwLock sync.RWMutex
}

func (stack *Stack) push(t int) {
	//Initialize items slice if not initialized
	if len(stack.items) == 0 {
		stack.items = []int{}
	}

	// Acquire read, write lock before inserting a new item in the stack.
	stack.rwLock.Lock()

	// Performs append operation.
	stack.items = append(stack.items, t)
	// This will release read, write lock
	stack.rwLock.Unlock()
}

// Pop removes an Item from the top of the stack
func (stack *Stack) Pop() *int {
	// Checking if stack is empty before performing pop operation
	if len(stack.items) == 0 {
		return nil
	}
	// Acquire read, write lock as items are going to modify.
	stack.rwLock.Lock() // it acquires a write lock to avoid dirty reads by other threads.
	// Popping item from items slice.
	item := stack.items[len(stack.items)-1]
	//Adjusting the item's length accordingly
	stack.items = stack.items[0 : len(stack.items)-1]
	// Release read write lock.
	stack.rwLock.Unlock()
	// Return last popped item
	return &item
}

// Size return size i.e. number of items present in stack.
func (stack *Stack) Size() int {
	// Acquire read lock
	stack.rwLock.RLock()
	// defer operation of unlock.
	defer stack.rwLock.RUnlock()
	// Return length of items slice.
	return len(stack.items)
}

// All - return all items present in stack
func (stack *Stack) All() []int {
	// Acquire read lock
	stack.rwLock.RLock()
	// defer operation of unlock.
	defer stack.rwLock.RUnlock()
	// Return items slice to caller.
	return stack.items
}

// IsEmpty - Check is stack is empty or not.
func (stack *Stack) IsEmpty() bool {
	// Acquire read lock
	stack.rwLock.RLock()
	// defer operation of unlock.
	defer stack.rwLock.RUnlock()
	return len(stack.items) == 0
}

func main() {
	s := Stack{}
	s.push(10)
	fmt.Println(s.items, s.Size, s.IsEmpty)     // [10] 0x499600 0x499660 // doubt why it returns address?
	fmt.Println(s.items, s.Size(), s.IsEmpty()) // [10] 1 false

	s.Pop()
	fmt.Println(s.items, s.Size(), s.IsEmpty()) // [10] 1 false
}
