package main

import (
	"fmt"
	"strings"
)

func join(strs ...string) string {
	var ret string
	for _, str := range strs {
		ret += str + " "
	}
	return ret
}

func joinUsingStringBuilder(strs ...string) string {
	var sb strings.Builder
	for _, str := range strs {
		sb.WriteString(str + " ")
	}
	return sb.String()
}

func joinRunes(runes ...rune) string {
	var sb strings.Builder
	for _, r := range runes {
		sb.WriteRune(r)
	}
	return sb.String()
}

func main() {
	fmt.Println("String Concatenation Tutorial")

	var myString strings.Builder
	// we can use the WriteString method to append
	// to our existing strings.Builder string
	myString.WriteString("Hello ")

	// here we append to the end of our string
	myString.WriteString("World")

	// print out our concatenated string
	fmt.Println(myString.String())
	myString.Reset()
	fmt.Println("Empty string builder", myString)

	fmt.Print(join("Mayank", "Sahu", "Class", "sixth"))
	fmt.Println("")

	fmt.Print(joinRunes("Mayank", "Sahu", "Class", "sixth"))
	fmt.Println("")

	// r1 := []rune{"aa"}
	// r2 := []rune{"Mayank"}
	// r3 := []rune{"Mayank"}
	// r4 := []rune{"Mayank"}

	// fmt.Print(joinRunes(r1, r2))
	// fmt.Println("")

}
