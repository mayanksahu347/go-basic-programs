/*
- bool
- string
- Numeric Types
int8, int16, int32, int64, int
uint8, uint16, uint32, uint64, uint
float32, float64
complex64, complex128
byte
rune
*/

package main

import (  
    "fmt"
    "unsafe"
)

func main() {  
    a := true
    b := false
    fmt.Println("a:", a, "b:", b)
    c := a && b
    fmt.Println("c:", c)
    d := a || b
	fmt.Println("d:", d)
	

	var a1 int = -99
    b1 := 95
	fmt.Println("value of a1 is", a1, "and b1 is", b1)
    fmt.Printf("type of a1 is %T, size of a1 is %d bytes", a1, unsafe.Sizeof(a1)) //type and size of a
	fmt.Printf("\ntype of b1 is %T, size of b1 is %d bytes", b1, unsafe.Sizeof(b1)) //type and size of b
	
	//uint size depends in platform
	var a2 uint = 89
    b2 := 95
	fmt.Println("\nvalue of a2 is", a2, "and b2 is", b2)
    fmt.Printf("type of a2 is %T, size of a2 is %d bytes", a2, unsafe.Sizeof(a2)) //type and size of a
	fmt.Printf("\ntype of b2 is %T, size of b2 is %d bytes", b2, unsafe.Sizeof(b2)) //type and size of b
	
	//float size depends on platform
	var a3 float32 = 89.8
    b3 := 95
	fmt.Println("\nvalue of a3 is", a3, "and b3 is", b3)
    fmt.Printf("type of a3 is %T, size of a3 is %d bytes", a3, unsafe.Sizeof(a3)) //type and size of a
	fmt.Println("\ntype of b3 is %T, size of b3 is %d bytes", b3, unsafe.Sizeof(b3)) //type and size of b
	

	a4, b4 := 5.67, 8.97
	fmt.Printf("type of a4 %T, b4 %T", a4, b4)
    sum := a4 + b4
    diff := a4 - b4
    fmt.Println("\nsum", sum, "diff", diff)

    no1, no2 := 56, 89
	fmt.Println("sum", no1+no2, "diff", no1-no2)
	

	// type complex number with real and imagenary parts
	var c1 = complex(5, 7)
    c2 := 8 + 27i
    cadd := c1 + c2
    fmt.Println("sum:", cadd)
    cmul := c1 * c2
	fmt.Println("product:", cmul)
	
	// type string
	first := "Naveen"
    last := "Ramanathan"
    name := first +" "+ last
	fmt.Println("My name is",name)
	
	// type conversion
	i1 := 55      //int
    j1 := 67.8    //float64
    // sum1 := i1 + j1 //int + float64 not allowed
	// fmt.Println(sum1)
	
	i1 = 110     //int
    j1 = 67.8    //float64
    sum2 := i1 + int(j1) //int + float64 not allowed
    fmt.Println(sum2)
}