package main

import "fmt"
import "math"

func main() {
	var age = 10// variable declaration
	fmt.Println("My age is", age)
	age = 29 //assignment
	fmt.Println("My age is", age)
	age = 54 //assignment
	fmt.Println("My new age is", age)

	var inferred_age = 29 // type will be inferred
	fmt.Println("My inferred age is", inferred_age)
	
	//declaring multiple variables with type

	var width, height int = 100, 60 //declaring multiple variables

	fmt.Println("width is", width, "height is", height)
	
	//declaring multiple variables without type
	var infered_width, infered_height = 100, 50 //"int" is dropped

	fmt.Println("width is", infered_width, "height is", infered_height)
	
	var (
        name1   = "naveen"
        age1    = 29
        height1 int
    )
    fmt.Println("my name is", name1, ", age is", age1, "and height is", height1)
	
	// Shorthand decleration
	count := 10
	fmt.Println("Count =",count)
	
	// Multiple shorthand decleration
	name2, age2 := "naveen", 29 //short hand declaration

	fmt.Println("my name is", name2, "age is", age2)
	
	//  * Short hand declaration requires initial values for all variables on the left-hand side of the assignment. 
	//  name, age := "naveen" //error
	

	// * Short hand syntax can only be used when at least one of the variables on the left side of := is newly declared.
	a, b := 20, 30 // declare variables a and b
    fmt.Println("a is", a, "b is", b)
    b, c := 40, 50 // b is already declared but c is new
    fmt.Println("b is", b, "c is", c)

	// math package
	a1, b1 := 145.8, 543.8
    c1 := math.Min(a1, b1)
	fmt.Println("Minimum value is", c1)
	
	// Since Go is strongly typed, variables declared as belonging to one type cannot be assigned a value of another type. 
	// age3 := 29      // age is int
    // age3 = "naveen" // error since we are trying to assign a string to a variable of type int

	//
}
