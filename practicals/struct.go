/*
A struct is a user-defined type that represents a collection of fields. It can be used in places where it makes sense to group the data into a single unit rather than having each of them as separate values.
*/

package main

import (
	"fmt"
	"structs/computer"
)

// decleration
type Employee struct {
	firstName string
	lastName  string
	age       int
}

// decleraing anonynous struct
type Person struct {
	string
	int
}

type Address struct {
	city, state string
}
type Human struct {
	name    string
	age     int
	address Address
}

type PromotedField struct {
	DOB    string
	height int
	Address
}

func main() {
	//creating struct specifying field names
	var e1 Employee
	e1.age = 10
	e1.firstName = "Mayank"
	e1.lastName = "Sahu"

	// Shorthand for struct
	e2 := Employee{
		firstName: "Guddu",
		lastName:  "Bhaiya",
		age:       25,
	}

	// creating anonymous struct
	e3 := struct {
		FullName, dept string
		salary         int
	}{
		FullName: "Radhe",
		salary:   10000,
		dept:     "IT",
	}
	fmt.Println("Employee 1", e1)
	fmt.Println("Employee 2", e2)
	fmt.Println("Employee 2", e3)

	// Accessing individual fields of struct
	fmt.Println("Employee 1 firstname", e1.firstName)
	fmt.Println("Employee 1 lastname", e1.lastName)
	fmt.Println("Employee 1 age", e1.age)

	// Pointers to a struct
	e4 := &e3
	_ = e4
	fmt.Println("Employee 3 fullname", (*e4).FullName)
	fmt.Println("Employee 3 department", (*e4).dept)
	fmt.Println("Employee 3 salary", (*e4).salary)

	fmt.Println("Employee 3 fullname", (e4).FullName)
	fmt.Println("Employee 3 department", (e4).dept)
	fmt.Println("Employee 3 salary", (e4).salary)

	fmt.Println("Employee 3 fullname", e4.FullName)
	fmt.Println("Employee 3 department", e4.dept)
	fmt.Println("Employee 3 salary", e4.salary)

	p1 := Person{"Mayank", 78}
	p2 := Person{string: "ABCD", int: 70}
	fmt.Println("Person 1", p1)
	fmt.Println("Person 2", p2)

	// nested struct
	h1 := Human{
		name: "Pappu",
		age:  25,
		address: Address{
			city:  "bhopal",
			state: "MP",
		},
	}

	fmt.Println("Human 1", h1)

	// Promoted fields
	pf1 := PromotedField{
		DOB:    "08/09/1996",
		height: 185,
		Address: Address{
			city:  "bhopal",
			state: "MP",
		},
	}
	fmt.Println("Promoted Field 1", pf1)
	fmt.Println("Promoted Field can access city: ", pf1.city)

	// imported struct
	c1 := computer.Spec{
		Maker: "Dell",
		Price: 50000,
	}

	fmt.Println("Imported struct ", c1)

	// Struct are value types and are comparable if each of their fields are comparable. Two struct variables are considered equal if their corresponding fields are equal.

	//Struct variables are not comparable if they contain fields that are not comparable like maps

}
