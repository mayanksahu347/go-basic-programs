package main

import (
	"fmt"
)

func find(num int, nums ...int) {
	fmt.Printf("type of nums is %T\n", nums)
	found := false
	for i, v := range nums {
		if v == num {
			fmt.Println(num, "found at index", i, "in", nums)
			found = true
		}
	}
	if !found {
		fmt.Println(num, "not found in ", nums)
	}
	fmt.Printf("\n")
}

func main() {
	// The way variadic functions work is by converting the variable number of arguments to a slice of the type of the variadic parameter. For instance, in line no. 22 of the program above, the variable number of arguments to the find function are 89, 90, 95. The find function expects a variadic int argument. Hence these three arguments will be converted by the compiler to a slice of type int []int{89, 90, 95} and then it will be passed to the find function.

	find(89, 89, 90, 95)
	find(45, 56, 67, 45, 90, 109)
	find(78, 38, 56, 98)
	find(87)

	// Slice arguments vs Variadic arguments
	// - I personally feel that the program with variadic functions is more readable than the once with slices

	// * Append is a variadic function
	// func append(slice []Type, elems ...Type) []Type

	/* * We cannot pass a slice directly to a variadic function but There is a syntactic sugar which can be used to pass a slice to a variadic function.
		nums := []int{89, 90, 95}
		find(89, nums) //wrong
		find(89, nums...) //right
	*/

}
