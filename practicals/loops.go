// The variables declared in a for loop are only available within the scope of the loop. Hence i cannot be accessed outside the body for loop.

package main

import (
	"fmt"
)

func main() {
	for i := 1; i <= 10; i++ {
		if i > 5 {
			break //loop is terminated if i > 5
		}
		fmt.Printf("%d ", i)
	}
	fmt.Printf("\nline after for loop\n")

	for i := 1; i <= 10; i++ {
		if i%2 == 0 {
			continue
		}
		fmt.Printf("%d \n", i)
	}

	// Labels can be used to break the outer loop from inside the inner for loop. Let's understand what I mean by using a simple example.

outer:
	for i := 0; i < 3; i++ {
		for j := 1; j < 4; j++ {
			fmt.Printf("i = %d , j = %d\n", i, j)
			if i == j {
				break outer
			}
		}

	}

	// loop variation-1
	i := 0
	for i <= 10 { // initialization and post are omitted
		fmt.Printf("%d ", i)
		i += 2
	}

	// multiple variables in loop
	for no, i := 10, 1; i <= 10 && no <= 19; i, no = i+1, no+1 {
		fmt.Printf("%d * %d = %d\n", no, i, no*i)
	}
}
