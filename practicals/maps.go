/*
- A map can be created by passing the type of key and value to the make function. The following is the syntax to create a new map.
- Similar to slices, maps are reference types. When a map is assigned to a new variable, they both point to the same internal data structure. Hence changes made in one will reflect in the other.

make(map[type of key]type of value)
employeeSalary := make(map[string]int)
*/

package main

import (
	"fmt"
)

type Service interface {
	SayHi()
}

type MyService struct{}

func (s MyService) SayHi() {
	fmt.Println("Hi")
}

type SecondService struct{}

func (s SecondService) SayHi() {
	fmt.Println("Hello From the 2nd Service")
}

func main() {
	employeeSalary := make(map[string]int)
	employeeSalary["steve"] = 12000
	employeeSalary["jamie"] = 15000
	employeeSalary["mike"] = 9000
	fmt.Println("employeeSalary map contents:", employeeSalary)

	// initialize during decleration
	employeeSalary1 := map[string]int{
		"steve": 12000,
		"jamie": 15000,
	}
	employeeSalary1["mike"] = 9000
	fmt.Println("employeeSalary1 map contents:", employeeSalary1)

	// Zero value of map
	// var employeeSalary2 map[string]int
	// employeeSalary2["steve"] = 12000

	// Retrieving value for a key from a map
	employeeSalary3 := map[string]int{
		"steve": 12000,
		"jamie": 15000,
		"mike":  9000,
	}
	employee := "jamie"
	salary := employeeSalary3[employee]
	fmt.Println("Salary of", employee, "is", salary)

	// Checking if a key exists
	newEmp := "steve"
	value, isPresent := employeeSalary3[newEmp]
	if isPresent {
		fmt.Println("Salary of", newEmp, "is", value)
	}

	// iterate over maps
	fmt.Println("Contents of the map")
	for key, value := range employeeSalary3 {
		fmt.Printf("employeeSalary[%s] = %d\n", key, value)
	}

	// Deleting items from a map
	delete(employeeSalary3, "steve")
	fmt.Println(employeeSalary3) // steve removed

	// Map of structs

	// Length of the map
	fmt.Println("length is", len(employeeSalary3))

	// value is an interface

	fmt.Println("Go Maps Tutorial")
	// we can define a map of string uuids to
	// the interface type 'Service'
	interfaceMap := make(map[string]Service)

	// we can then populate our map with
	// simple ids to particular services
	interfaceMap["SERVICE-ID-1"] = MyService{}
	interfaceMap["SERVICE-ID-2"] = SecondService{}

	// Incoming HTTP Request wants service 2
	// we can use the incoming uuid to lookup the required
	// service and call it's SayHi() method
	interfaceMap["SERVICE-ID-2"].SayHi()

	// iterate over maps
	for key, service := range interfaceMap {
		fmt.Println(key)
		service.SayHi()
	}

}
