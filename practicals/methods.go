package main

import (
	"fmt"
	"math"
)

type Employee struct {
	name     string
	salary   int
	currency string
}

type Rectangle struct {
	length int
	width  int
}

type Circle struct {
	radius float64
}

/*
 displaySalary() method has Employee as the receiver type
*/
func (e Employee) displaySalary() {
	fmt.Printf("Salary of %s is %s%d", e.name, e.currency, e.salary)
}

func (r Rectangle) Area() int {
	return r.length * r.width
}

func (c Circle) Area() float64 {
	return math.Pi * c.radius * c.radius
}

func (e Employee) changeName(newName string) {
	e.name = newName
}

/*
Method with pointer receiver
*/
func (e *Employee) changeSalary(newSalary int) {
	e.salary = newSalary
}

func main() {
	emp1 := Employee{
		name:     "Sam Adolf",
		salary:   5000,
		currency: "$",
	}
	emp1.displaySalary() //Calling displaySalary() method of Employee type

	/* So why do we have methods when we can write the same program using functions. There are a couple of reasons for this. Let's look at them one by one.
	   1. Go is not a pure object-oriented programming language and it does not support classes. Hence methods on types are a way to achieve behavior similar to classes
	   2. Methods with the same name can be defined on different types whereas functions with the same names are not allowed.
	*/

	// methods with same name
	r := Rectangle{
		length: 10,
		width:  5,
	}
	fmt.Printf("\nArea of rectangle %d\n", r.Area())
	c := Circle{
		radius: 12,
	}
	fmt.Printf("Area of circle %f", c.Area())

	// pointer receiver
	e := Employee{
		name:   "Mark Andrew",
		salary: 50000,
	}
	fmt.Printf("\nEmployee name before change: %s", e.name)
	e.changeName("Michael Andrew")
	fmt.Printf("\nEmployee name after change: %s", e.name)

	fmt.Printf("\n\nEmployee salary before change: %d", e.salary)
	e.changeSalary(51000)
	fmt.Printf("\nEmployee salary after change: %d", e.salary)

    // Generally, pointer receivers can be used when changes made to the receiver inside the method should be visible to the caller.
}
