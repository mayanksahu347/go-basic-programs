/*
- Go does not support inheritance, however it does support composition. The generic definition of composition is "put together". One example of composition is a car. A car is composed of wheels, engine and various other parts.
- Composition can be achieved in Go is by embedding one struct type into another.
*/

package main

import "fmt"

type author struct {
	firstname string
	lastname  string
	bio       string
}

type post struct {
	title   string
	content string
	author
}

type website struct {
	posts []post
}

func (a author) fullName() string {
	return fmt.Sprintf("%s %s", a.firstname, a.lastname)
}

func (p post) details() {
	fmt.Println("title", p.title)
	fmt.Println("content", p.content)
	fmt.Println("author firstname", p.author.firstname)
	fmt.Println("author lastname", p.author.lastname)
	fmt.Println("author bio", p.author.bio)
	fmt.Println("author fullname", p.author.fullName())
}

func (p post) details1() {
	fmt.Println("title", p.title)
	fmt.Println("content", p.content)
	fmt.Println("author firstname", p.firstname)
	fmt.Println("author lastname", p.lastname)
	fmt.Println("author bio", p.bio)
	fmt.Println("author fullname", p.fullName())
}

func (w website) contents() {
	fmt.Println("Contents of Website\n")
	for _, v := range w.posts {
		v.details()
		fmt.Println()
	}
}

func main() {
	a1 := author{
		firstname: "Mayank",
		lastname:  "Sahu",
		bio:       "developer",
	}

	a2 := author{
		firstname: "Gaurav",
		lastname:  "agrawal",
		bio:       "writer",
	}

	p1 := post{
		title:   "Dark Owl",
		content: "Nice",
		author:  a1,
	}

	p1.details()
	p1.details1()

	// Embedding slice of structs

	post1 :=
		post{
			title:   "Dark Owl",
			content: "Nice",
			author:  a1,
		}
	post2 :=
		post{
			title:   "abcd",
			content: "good",
			author:  a1,
		}
	post3 :=
		post{
			title:   "history",
			content: "very good",
			author:  a2,
		}

	posts := []post{post1, post2, post3}
	w1 := website{
		posts,
	}
	w1.contents()
}
