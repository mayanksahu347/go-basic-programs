package main

import (
	"encoding/json"
	"fmt"
)

type Message struct {
	Name string
	Body string
	Time int64
}

func addKey(key string, value interface{}, data []byte) ([]byte, error) {
	dataMap := make(map[string]interface{})
	err := json.Unmarshal(data, &dataMap)
	if err != nil {
		return nil, err
	}

	dataMap[key] = value
	return json.Marshal(dataMap)
}

func main() {
	// marshal
	m := Message{"Alice", "Hello", 1294706395881547000}

	b1, err := json.MarshalIndent(m, "", "  ")

	if err != nil {

	}

	fmt.Println(string(b1))

	// unmarshal
	var m1 Message

	b := []byte(`{"Name":"Bob","Food":"Pickle"}`)

	err = json.Unmarshal(b, &m1)

	if err != nil {

	}

	fmt.Println(m1)

	var i interface{}
	i = "a string"
	i = 20
	// i = 2.7

	r := i.(int)
	fmt.Println("the circle's area", r*r)
	// ---------------------------------------------------------------
	/*
		bb := []byte(`{"Name":"Wednesday","Age":6,"Parents":["Gomez","Morticia"]}`) //json
		// doubt
		// var f interface{}      this works
		// var f []interface{}		what is this
		var f interface{}
		err = json.Unmarshal(bb, &f)
		fmt.Printf("%T\n", f)
		fmt.Println("without", f)

		// fmt.Println(f["Name"]) // will not work doubt why?

		// type assertion ke baad works
		mm := f.(map[string]interface{})
		fmt.Printf("%T\n", mm)
		fmt.Println("with assertion", mm)
		fmt.Println(mm["Name"])
		mm["Salary"] = 1000
		// fmt.Println("with assertion", mm)
	*/

}
