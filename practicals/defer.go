/*
stack of defers
- When a function has multiple defer calls, they are pushed on to a stack and executed in Last In First Out (LIFO) order.

- practical use
Defer is used in places where a function call should be executed irrespective of the code flow
*/
package main

import (
	"fmt"
)

func finished() {
	fmt.Println("Finished finding largest")
}

type person struct {
	firstName string
	lastName  string
}

func (p person) fullName() {
	fmt.Printf("%s %s\n", p.firstName, p.lastName)
}

func largest(nums []int) {
	defer finished()
	fmt.Println("Started finding largest")
	max := nums[0]
	for _, v := range nums {
		if v > max {
			max = v
		}
	}
	fmt.Println("Largest number in", nums, "is", max)
}

func printA(a int) {
	fmt.Println("value of a in deferred function", a)
}

func main() {
	// defer function
	nums := []int{78, 109, 2, 563, 300}
	largest(nums)

	// defer methods
	p := person{
		firstName: "John",
		lastName:  "Smith",
	}

	defer p.fullName()
	fmt.Printf("Welcome \n")

	// argument evaluation
	a := 5
	defer printA(a)
	a = 10
	fmt.Println("value of a before deferred function call", a)
}
