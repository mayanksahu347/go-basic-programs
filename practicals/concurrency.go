/*
- It redefines what it means to build concurrent programs by providing us with these goroutines and channels.
- They enable us to create asynchronous parallel programs that can execute some tasks far quicker than if they were written in a sequential manner.
*/

package main

import (
	"fmt"
	"time"
)

// a very simple function that we'll
// make asynchronous later on
func compute(value int) {
	for i := 0; i < value; i++ {
		time.Sleep(time.Second)
		fmt.Println(i)
	}
}

func main() {
	fmt.Println("Goroutine Tutorial")

	// 1.
	// sequential execution of our compute function
	compute(5)
	compute(5)

	// we scan fmt for input and print that to our console
	var input string
	fmt.Scanln(&input)

	// 2.
	fmt.Println("Making program asyncronous")
	// notice how we've added the 'go' keyword
	// in front of both our compute function calls
	go compute(5) // if we try to execute this it would not print anything because
	go compute(5) //  our main function completed before our asynchronous functions could execute and as such, any goroutines that have yet to complete are promptly terminated.

	// added scanf so that our goroutine executes
	var input2 string
	fmt.Scanln(&input2)

	// 3.
	// Anonymous Goroutine Functions
	go func() {
		fmt.Println("Executing my Concurrent anonymous function")
	}()

	// added scanf so that our goroutine executes
	var input3 string
	fmt.Scanln(&input3)
}
