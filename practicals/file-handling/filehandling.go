package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"

	"github.com/gobuffalo/packr/v2"
)

func main() {
	/*
		READING FILES
	*/
	// One of the most basic file operations is reading an entire file into memory. This is done with the help of the ReadFile function of the ioutil package.
	// data, err := ioutil.ReadFile("test.txt") // reads the file and returns a byte slice which is stored in data
	// if err != nil {
	// 	fmt.Println("File reading error", err)
	// 	return
	// }
	// fmt.Println("Contents of file using basic:", string(data))

	// above cannot be run from the binary

	// 1. Using absolute file path
	data1, err1 := ioutil.ReadFile("/home/vishal/work/training/go-training/go-basic-programs/practicals/file-handling/test.txt") // reads the file and returns a byte slice which is stored in data
	if err1 != nil {
		fmt.Println("File reading error", err1)
		return
	}
	fmt.Println("Contents of file using absolute path:", string(data1))

	// 2. Passing the file path as a command line flag using flag package
	fptr := flag.String("fpath", "test.txt", "file path to read from")
	flag.Parse() // flag.Parse() should be called before any flag is accessed by the program.
	fmt.Println("value of fpath is", *fptr)

	data2, err2 := ioutil.ReadFile(*fptr)
	if err2 != nil {
		fmt.Println("File reading error", err2)
		return
	}
	fmt.Println("Contents of file using flag:", string(data2))

	// 3. Bundling the text file along with the binary
	// we are able to bundle the text file along with our binary

	box := packr.New("fileBox", "../filehandling") //  we are creating a New Box named box. A box represents a folder whose contents will be embedded in the binary.
	data3, err3 := box.FindString("test.txt")      // we read the contents of the file using the FindString method and print it.
	if err3 != nil {
		fmt.Println("File reading error", err3)
		return
	}
	fmt.Println("Contents of file using binary:", data3)

	/*
		WRITING FILES
	*/

	mydata := []byte("all my data I want to write to a file")

	// the WriteFile method returns an error if unsuccessful
	err := ioutil.WriteFile("myfile.data", mydata, 0777)
	// handle this error
	if err != nil {
		// print it out
		fmt.Println(err)
	}

	// checking whether data was written or not
	data, err := ioutil.ReadFile("myfile.data")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println("Data written to file:", string(data))

	// writing data to existing files
	f, err := os.OpenFile("myfile.data", os.O_APPEND|os.O_WRONLY, 0600) // open the file
	if err != nil {
		panic(err)
	}
	defer f.Close()

	if _, err = f.WriteString("  new data that wasn't there originally\n"); err != nil {
		// added the data
		panic(err)
	}

	data, err = ioutil.ReadFile("myfile.data")
	if err != nil {
		fmt.Println(err)
	}

	fmt.Print("Added data to existing data:", string(data))

}
