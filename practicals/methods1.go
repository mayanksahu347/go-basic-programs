package main

import "fmt"

type Rectangle struct {
	length, width int
}

func area(r Rectangle) float32 {
	area := r.length * r.width
	return float32(area)
}

func (r Rectangle) area() float32 {
	area := r.length * r.width
	r.length = 15
	r.width = 25
	return float32(area)

	/* values of length and width will not be updated, these will be updated when we receive as pointer in receiver like
	func (r *Rectangle) area() float32 {
	}
	*/
}

func perimeter(r *Rectangle) {
	fmt.Println("perimeter function output:", 2*(r.length+r.width))

}

func (r *Rectangle) perimeter() {
	fmt.Println("perimeter method output:", 2*(r.length+r.width))
}

func main() {
	// * Value receivers in methods vs Value arguments in functions
	r1 := Rectangle{
		length: 10,
		width:  20,
	}

	a1 := area(r1)
	fmt.Println(a1)

	// p := &r1
	// a2 := area(p)
	// fmt.Println(a2) // cannot pass pointer with argument

	var r2 Rectangle
	r2.length = 21
	r2.width = 34

	fmt.Println(r2.area())
	fmt.Println((&r2).area()) // can pass pointer with receiver

	// * Pointer receivers in methods vs Pointer arguments in functions

	r3 := Rectangle{
		length: 10,
		width:  5,
	}

	p3 := &r3 //pointer to r
	perimeter(p3)
	p3.perimeter()

	r3.perimeter() //calling pointer receiver with a value

	/* * Methods with non-struct receivers
	To define a method on a type, the definition of the receiver type and the definition of the method should be present in the same package.

	func (a int) add(b int) {
	}

	This is not allowed since the definition of the method add and the definition of type int is not in the same package. This program will throw compilation error cannot define new methods on non-local type int
	*/

	num1 := myInt(5)    // 1st way to declare variable
	var num2 myInt = 10 // 2nd way to declare variable
	sum := num1.add(num2)
	fmt.Println("Sum is", sum)
}

type myInt int

func (a myInt) add(b myInt) myInt {
	return a + b
}
