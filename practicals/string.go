package main

import (
	"fmt"
	"strconv"
	"unicode/utf8"
)

func printBytes(s string) {
	fmt.Printf("Bytes: ")
	for i := 0; i < len(s); i++ {
		fmt.Printf("%x ", s[i])
	}
}

func printChars(s string) {
	fmt.Printf("Characters: ")
	for i := 0; i < len(s); i++ {
		fmt.Printf("%c ", s[i])
	}
}

func printCharsUsingRune(s string) {
	fmt.Printf("Characters using rune: ")
	runes := []rune(s)
	for i := 0; i < len(runes); i++ {
		fmt.Printf("%c ", runes[i])
	}
}

func main() {
	name := "Hello World"
	fmt.Printf("String: %s\n", name)
	printChars(name)
	fmt.Printf("\n")
	printBytes(name)
	fmt.Printf("\n\n")
	name = "Señor"
	fmt.Printf("String: %s\n", name)
	printChars(name)
	fmt.Printf("\n")
	printCharsUsingRune(name)
	fmt.Printf("\n")
	printBytes(name)

	// A rune is a builtin type in Go and it's the alias of int32. Rune represents a Unicode code point in Go. It doesn't matter how many bytes the code point occupies, it can be represented by a rune. Let's modify the above program to print characters using a rune.

	// length of string
	word1 := "Señor"
	fmt.Printf("\n\nString: %s\n", word1)
	fmt.Printf("Length: %d\n", utf8.RuneCountInString(word1))
	fmt.Printf("Number of bytes: %d\n", len(word1))

	fmt.Printf("\n")
	word2 := "Pets"
	fmt.Printf("String: %s\n", word2)
	fmt.Printf("Length: %d\n", utf8.RuneCountInString(word2))
	fmt.Printf("Number of bytes: %d\n", len(word2))

	// String concatenation
	string1 := "Go"
	string2 := "is awesome"
	result := string1 + " " + string2
	fmt.Println(result)

	result1 := fmt.Sprintf("%s %s", string1, string2)
	fmt.Println(result1)

	// Strings are immutable in Go. Once a string is created it's not possible to change it.
	// h := "hello"
	// h[0] = 'a' // wrong

	// To workaround this string immutability, strings are converted to a slice of runes. Then that slice is mutated with whatever changes are needed and converted back to a new string.

	str := "hand"
	str = mutate([]rune(str))
	fmt.Println(str)

	// convert string to int
	myAgeString := "23"
	fmt.Printf("My Age: %s\n", myAgeString)

	ageValue, err := strconv.Atoi(myAgeString)
	if err != nil {
		fmt.Println("Error:", err)
	}

	ageValue += 1
	fmt.Printf("New Age: %d\n", ageValue)

	// convert int to string
	fmt.Println("integer to string conversion")

	var ourString string

	ourString = strconv.Itoa(12345)

	fmt.Println(ourString)
}

func mutate(s []rune) string {
	s[0] = 'n'
	return string(s)
}
