/*
- Go does not provide classes but it does provide structs. Methods can be added on structs. This provides the behaviour of bundling the data and methods that operate on the data together akin to a class.
- Go doesn't support constructors
-
*/
package main

import "oop/employee"

func main() {
	// basic
	e := employee.Employee{
		FirstName:   "Sam",
		LastName:    "Adolf",
		TotalLeaves: 30,
		LeavesTaken: 20,
	}
	e.LeavesRemaining()

	// with zero values without new function
	var e1 employee.Employee
	e1.LeavesRemaining()

	e2 := employee.New("Unexported Sam", "Adolf", 30, 20)
    e2.LeavesRemaining()
}
