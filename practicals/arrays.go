// Mixing values of different types, for example an array that contains both strings and integers is not allowed in Go.

package main

import "fmt"

func changeLocal(num [5]int) {
	num[0] = 55
	fmt.Println("inside function ", num)

}

func main() {

	// array decleration
	var a [2]int
	fmt.Println(a)

	// shorthand decleration
	b := [3]int{}
	fmt.Println(b)

	c := [3]int{12, 13, 13}
	fmt.Println(c)

	// not providing length
	d := [...]int{}
	fmt.Println(d)

	// Arrays in Go are value types and not reference types. This means that when they are assigned to a new variable, a copy of the original array is assigned to the new variable. If changes are made to the new variable, it will not be reflected in the original array.

	// Similarly when arrays are passed to functions as parameters, they are passed by value and the original array in unchanged.

	num := [...]int{5, 6, 7, 8, 8}
	fmt.Println("before passing to function ", num)
	changeLocal(num) //num is passed by value
	fmt.Println("after passing to function ", num)

	// length of an array
	fmt.Println("length of d is", len(d))

	// iterate using for loop
	aa := [...]float64{67.7, 89.8, 21, 78}
	for i := 0; i < len(aa); i++ { //looping from 0 to the length of the array
		fmt.Printf("%d th element of a is %.2f\n", i, aa[i])
	}

	// iterate using range
	bb := [...]float64{61.7, 9.8, 2.1, 84}
	sum := float64(0)
	for i, v := range bb { //range returns both the index and value
		fmt.Printf("%d the element of bb is %.2f\n", i, v)
		sum += v
	}
	fmt.Println("\nsum of all elements of bb", sum)

	// ignores index while using range
	// for _, v := range a {
	// }

	// multidimensional array
	r := [3][2]string{
		{"lion", "tiger"},
		{"cat", "dog"},
		{"pigeon", "peacock"}, //this comma is necessary. The compiler will complain if you omit this comma
	}
	fmt.Println(r[2][1])
}
